# medicalinformatics-covid19

# [Bitte Teilnehmen: Umfrage zu MI-Aktivitäten (Register, Tools, Datenanalysen und -integration)](https://soscisurvey.ukaachen.de/sosci/covid/) 

Sammlung von Infos, Tools und Aktivitäten für medizininformatische Unterstützung der Covid-19-Krise. Das Wiki wurde vom [Institut für Medizinische Informatik der Universitätsmedizin Göttingen](https://medizininformatik.umg.eu) eingerichtet
und wird von weiteren Freiwilligen aus der MI-Community mitbetreut. Die Infos finden die beste Verbreitung über die Mailingliste mi-covid19@medizin.uni-goettingen.de (Anmeldung mit einem Uni-Account unter https://listserv.gwdg.de/mailman/listinfo/mi-covid19). 
Wer nur Infos senden will, ohne selber Infos über die Mailingliste empfangen zu wollen, kann diese auch an das Helpdesk dieses Wikis senden: gitlab+medinfpub-medicalinformatics-covid19-10607-issue-@gwdg.de.  



# COVID-19 Tools und Medizininformatischer Support

## Existierende Tools


| Was  | Wo | Wer | Woher |
|------|----|-----|-------|
| App zur Unterstützung der Anamnese und Verlaufsdokumentation bei Corona | https://hippokrates-it.de/corona/   |Medizininformatik Münster | MD |
| IOS-App zur Dokumentation des Krankheitsverlaufs|https://devpost.com/software/corona-app-feldtest|Kairos|ST|
| openEHR-Meldeunterstützung|https://github.com/AppertaFoundation/COVID-19-screening-interface/tree/develop| Apperta Foundation| BH| 
| Ressourcenplanung für Kliniken | http://predictivehealthcare.pennmedicine.org/2020/03/14/accouncing-chime.html  | Penn Medicine    |   twitter  "covid informatics"   |
| Fragebogen für die Einschätzung eines Verdachts |https://covapp.charite.de/| Charité| ST |
| Fragebogen zur Patienten-Selbstanamnese | www.selbstanamnese.de | NursIT Institute GmbH | email |
| Contact Tracing | https://www.pepp-pt.org/ | European Consortium | email |


## Terminologien und Standards

| Was  | Wo | Wer | Woher | Infos |
|------|----|-----|-------|-------|
| ICD-Codes: ICD-10: U07.1 ICD-11: RA01.0 |  https://www.who.int/classifications/icd/covid19/en/ | WHO | twitter  "covid informatics" ||
| SNOMED CT Extension | http://www.snomed.org/news-and-events/articles/march-2020-interim-snomedct-release-COVID-19 | SNOMED international | twitter  "covid informatics" | |
| US CDC - Data dictionary and report forms | https://www.cdc.gov/coronavirus/2019-ncov/php/reporting-pui.html| US-Behörde| Internetrecherche ||
| WHO Case Report Forms |https://apps.who.int/iris/bitstream/handle/10665/331234/WHO-2019-nCoV-SurveillanceCRF-2020.2-eng.pdf| WHO| Internetreche||
| WHO Data dictionary |https://www.who.int/docs/default-source/coronaviruse/data-dictionary-covid-crf-v6.xlsx?sfvrsn=a7d4ef98_2|WHO| Internetrecherche||
| Kontaktpersonenliste |https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Kontaktperson/Kontaktpersonenliste.xlsx?__blob=publicationFile| RKI| covid-support-chat ||
| Kurzfragebogen für Kontaktpersonen|https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Kontaktperson/Tagebuch_Kontaktpersonen.docx?__blob=publicationFile|[RKI](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Kontaktperson/Dokumente_Tab.html)| covid-support-chat||
| openEHR Templates |https://openehr.org/ckm/incubators/1013.30.80|[openEHR](https://www.openehr.org/news_events/openehr_news/311) |openEHR |||
| Confirmed Covid-19 infection report  | https://openehr.org/ckm/templates/1013.26.271 ||| Epidemiologischer Bericht bei Bestätigung einer COVID-19-Infektion |
| Suspected COVID-19 risk assessment v0 | https://openehr.org/ckm/templates/1013.26.267 ||| Risikobewertung einer Covid-19-Infektion aus Screening oder Selbstbewertung (NHS in Großbritannien) |
| Suspected COVID-19 risk assessment v0.1 | https://openehr.org/ckm/templates/1013.26.280 ||| Siehe 2. Nur neuere Version! |
| AU COVID-19 Likelihood Assessment | https://openehr.org/ckm/templates/1013.26.273 ||| Screening-Bewertungsdatensatz für COVID-19, der vom Northern Territory, Australien verwendet wird |
| Datenmodell Anamnese Covid-19 | https://medical-data-models.org/40276 | MD | wird in der App aus Münster verwendet ||
| LOINC SARS Coronavirus 2 | https://loinc.org/sars-coronavirus-2/ | Regenstrief Institute | email||
| GECCO | [art-decor](https://art-decor.org/art-decor/decor-project--covid19f-), [simplifier geccocore](https://simplifier.net/guide/germancoronaconsensusdataset-implementationguide/geccocore) | Nationales Forschungsnetzwerk der Universitätsmedizin zu Covid-19 | email ||


## Hilfreiche Infos allgemeinerer Art

| Was  | Wo | Wer | Woher |
|------|----|-----|-------|
| FHIR-Basisprofile Deutsch | https://simplifier.net/guide/basisprofil-de-r4/home | HL7-D | ST |
| FHIR-IG Infektionsschutzmeldung (mit COVID19) | https://simplifier.net/Infektionsschutzmeldung/ | HL7-D | email |
| FHIR COVID-19 | https://simplifier.net/covid-19/ | Dedalus Healthcare Systems Group | email |
| Implementierungsleitfaden Meldepflichtige Krankheiten |http://wiki.hl7.de/index.php?title=IG:%C3%9Cbermittlung_meldepflichtiger_Krankheiten_Labormeldung | HL7 | ST |
| Förderung Aufbau eines Forschungsnetzwerks| https://www.bmbf.de/de/karliczek-wir-foerdern-nationales-netzwerk-der-universitaetsmedizin-im-kampf-gegen-covid-11230.html?pk_campaign=RSS&pk_kwd=Pressemeldung | BMBF | email |
| Auswertung der KV-Daten hinsichtlich der Regionen in Deutschland mit Risikogruppen | https://www.versorgungsatlas.de/themen/alle-analysen-nach-datum-sortiert/?tab=1&uid=110 | Versorgungsatlas | email |


## Zusammenstellungen

| Was  | Wo | Anmerkung | Woher |
|------|----|-----|-------|
| Telemedizinlösungen | https://hih-2025.de/corona/ | Bundesregierung | ST |
| Kuratierte Liste von github-Repos und weiterem | https://github.com/soroushchehresa/awesome-coronavirus  | Das meiste sind Visualisierungen der öffentlichen Daten |   github  |
| Nützliche Tools für die Corona-Krise | https://wirvsvirushackathon.org/ressourcen/ | vor allem für den privat/öffentlichen Bereich,z.B. Nachbarschaftshilfen | Internetrecherche |
| ZB MED COVID-19 HUB | https://covid-19.zbmed.de/ | Ressourcen von freier Literatur, aktuellen Quellen und Text-Mining-Korpora; interaktive, bioinformatische Werkzeuge | email |
| Bioinformatische Tools | https://github.com/virtual-biohackathons/covid-19-bh20/wiki/Resources | aktuellen Projekte in der Bioinformatik | DW |
| FAIRSharing Collection zu COVID-Resourcen | https://fairsharing.org/collection/COVID19Resources | Mit umfangreichen Meta-Daten aufbereitete und Filterfunktion versehene Sammlung von internationalen Datenbanken und Wissensbasen (Fokus auf FAIRe Daten | DW |
| Covid-19 Clinical Trials | https://covid-19.heigit.org/clinical_trials.html | | MII |
| Covid-19 Aktivitäten MII | https://www.medizininformatik-initiative.de/de/covid-19-forschungsuebersicht | Umfrage läuft noch | MII |

### Initiativen

| Was  | Wo | Wer | Woher | Infos |
|------|----|-----|-------|-------|
| Register für Covid-Erkrankungen | https://leoss.net/ | DGI, DGZI | ST, Ärzteblatt |
| IT-Lösungen für Corona | https://wirvsvirushackathon.org/ | Bundesregierung | medinfo-chat | 20.3.-23.3. |
| Global data sharing initiative | https://msdataalliance.com/covid-19/covid-19-and-ms-global-data-sharing-initiative/ | MSDA, msif |Plattform seit 31.03. online |
| Covid-19 modeling portal |https://midasnetwork.us/covid-19/ | MIDAS Coordination Center University of Pittsburgh | github "covid 19"| Data & Software Ressources |  
| Covid-19 i2b2 Collaboration | https://transmartfoundation.org/covid-19-call-to-action/ | Harvard | email | AGs zu Minimal Data Set, Data Sharing, Analytics |
| OpenAIRE | https://docs.google.com/document/d/15NcljJd_kYMu4-QJzr_swRKSm3h-E8PkD_3SIQoRIS0/edit || email ||
| Forschungsnetzwerk BMBF | [simplifier](https://simplifier.net/forschungsnetzcovid-19), https://www.bmbf.de/de/karliczek-wir-foerdern-nationales-netzwerk-der-universitaetsmedizin-im-kampf-gegen-covid-11230.html | BMBF und Charité | US | 150 Mio für Unikliniken, s. cocos |
| NFDI4Health | https://www.nfdi4health.de/index.php/about/ | DFG, in Begutachtung | UMG am Antrag beteiligt | Infos |
| NFDI4Microbiota | https://nfdi4microbiota.de/index.php/2020/03/25/the-nfdi4microbiota-consortium-helps-scientists-researching-sars-cov-2-and-other-viruses/ | NFDI4Microbiota / ZB MED | email | infrastructure, services and activities |
| Virus Outbreak Data Network (VODAN) | https://www.go-fair.org/implementation-networks/overview/vodan/ | CODATA, RDA, WDS, and GO FAIR | email | Implementation Network |
| RDA-COVID19 | https://www.rd-alliance.org/groups/rda-covid19 | Research Data Alliance (RDA) | email | development of guidelines on data sharing and re-use |
| COVID-19 host genetics initiative | https://www.covid19hg.org/ | human genetics community | email ||
| OHSDI Patient Characterisation study (Study-a-thon) | https://www.ohdsi.org/covid-19-updates/ | email |https://www.youtube.com/watch?v=VDsZiSzfgqM |
| cocos – Corona Component Standards | http://www.cocos.team/ | Health Innovation Hub | email ||


